﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rendezes_interfész
{
    class Program
    {
        class meccs : IComparable  //tehát összehasonlíthatónak kell lennie az elemeknek
        {
            public string kikjátszottak { set; get; }
            public int gólokelsőcsapat { set; get; }
            public int gólokmásodikcsapat { set; get; }
            public int CompareTo(object m)
            {
                meccs x = (meccs)m;
                if (gólokelsőcsapat + gólokmásodikcsapat > x.gólokelsőcsapat + x.gólokmásodikcsapat)
                    return -1;
                else if (gólokelsőcsapat + gólokmásodikcsapat == x.gólokelsőcsapat + x.gólokmásodikcsapat)
                    return 0;
                else return 1;
            }
            public override string ToString()
            {
                return kikjátszottak + ":" + gólokelsőcsapat + ":" + gólokmásodikcsapat;
            }
        }
        static void cserésrendezés<ElemTípus>(List<ElemTípus> adatok) where ElemTípus : IComparable
        {   //ElemTípussal paraméterezett
            ElemTípus csere;
            for (int i = 0; i < adatok.Count - 1; i++)
                for (int j = i + 1; j < adatok.Count; j++)
                {
                    if (adatok[i].CompareTo(adatok[j]) > 0) //-1 kisebb, 0 egyenlő, 1 nagyobb
                    {
                        csere = adatok[i]; adatok[i] = adatok[j]; adatok[j] = csere;
                    }
                }
        }
        static void kiírás<ElemTípus>(List<ElemTípus> adatok)
        {   //ElemTípussal paraméterezett
            foreach (ElemTípus elem in adatok)
            {
                Console.Write(elem.ToString() + ",");
            }
            Console.WriteLine();
        }
        static void Main(string[] args)
        {
            List<int> egészek = new List<int>() { 1, 3, 2, 7, 4, 9, 0 };
            List<string> szövegek = new List<string>() { "Tamás", "Adina", "Zsolt", "Eszter", "Zoltán" };
            List<meccs> meccsek = new List<meccs>(){
                new meccs(){kikjátszottak="a-b", gólokelsőcsapat=2, gólokmásodikcsapat=1},
                new meccs(){kikjátszottak="a-c", gólokelsőcsapat=1, gólokmásodikcsapat=1},
                new meccs(){kikjátszottak="b-c", gólokelsőcsapat=1, gólokmásodikcsapat=0},
            };
            Console.Write("Rendezetlenül: ");
            kiírás<int>(egészek);
            cserésrendezés<int>(egészek);
            Console.Write("Rendezetten: ");
            kiírás<int>(egészek);
            Console.Write("Rendezetlenül: ");
            kiírás<string>(szövegek);
            cserésrendezés<string>(szövegek);
            Console.Write("Rendezetten: ");
            kiírás<string>(szövegek);
            //nemcsak beépített típusra használható megoldás
            Console.Write("Rendezetlenül: ");
            kiírás<meccs>(meccsek);
            cserésrendezés<meccs>(meccsek);
            Console.Write("Rendezetten: ");
            kiírás<meccs>(meccsek);
           
        }
    }
}
